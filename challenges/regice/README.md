# Regice

## Index

[[_TOC_]]

## Points

* Difficulty: Easy
* Maximum Score `150`
* Minimum Score `50`
* Decadence `15`

## Hints

None

## Flag

`bitup21{475bc4726804cb50667f4d097f428da7}`

## Attachments

* capture.zip (MD5: 0626eecb0e3db5b150a47b58a562bc8c)
    * capture.pcap (MD5: 702aacb5421493cfa63544da47d9d5a1)

## Deploy

None 

## Requirements

None

## Description

A compromise of one of the company's web servers has been detected. We have managed to recover a traffic capture made at the time of the compromise. By analyzing the information contained in the PCAP you will be able to see what happened, what was exfiltrated and thus get the flag!

## Writeup

The first thing we find once we open the .pcap file, are several types of traffic, both TCP, UDP, ICMP and DNS.

When analyzing the TCP traffic seen at the beginning of the capture, the following TCP frame can be observed.

![](images/1.png)

![](images/7.png)

Looking at the "printable strings" of the plot and the _magic numbers_, it can be seen that it may be a ZIP file.

To download the file, select _show data as: **Raw**_. At this point, the file is downloaded using the _save as_ button. 

Using the operating system's own utilities such as _file_, it can be seen that it is indeed a ZIP file, and it is corrupt.

![](images/8.png)

When looking at the _magic numbers_, the file is found to be corrupt.

![](images/9.png)

With tools to modify the hexadecimal content of a file, we can "fix" the ZIP to be able to decompress it. 

![](images/10.png)

To decompress it, we are asked for a password.

When analyzing the TCP traffic seen in the document, a privilege escalation and the writing of a file called "_secret.txt_" is detected.

![](images/3.png)

We can unzip the ZIP file with this password.

If we analyze the compressed _security.reg_ file, we can see that a registry key is created under CurrentVersion\Run of the user's Hive, which from PowerShell downloads a file hosted in _pastebin_.

![](images/11.png)

When we log in, we are prompted for a password.

If we continue to check the network, we find 2 **UDP** packets that we can analyze again with Wireshark.

![](images/4.png)

![](images/12.png)

It looks like we are dealing with a hexadecimal string.

With online tools or the operating system's own utilities, it can be decoded, which gives us another supposed password.

![](images/13.png)

If we put this string in the _pastebin_ link previously found, it returns our flag.

![](images/14.png)

## Notes

None

## References

None

## Author

@DreSecX
