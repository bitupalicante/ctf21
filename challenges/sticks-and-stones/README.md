# Sticks and Stones

## Index

[[_TOC_]]

## Points

* Difficulty: Hard
* Maximum Score `500`
* Minimum Score `150`
* Decadence `15`

## Hints

None

## Flag

`bitup21{ponemos_un_CRC_y_pasa_esto__sale_mal}`

## Attachments

* sticks_and_stones.zip (MD5: cb0077d987e668b83a37662c0ac41bf9)
    * sticks_and_stones.exe (MD5: 7f9d016af61a848cdbbc04e57c1ecc38)

## Deploy

None

## Requirements

None

## Description

Patches and nops may break my code, but any miserable try and you’ll be gone!

## Writeup

After executing the exe, we are welcomed with the following output:

![](images/1.png)

To get to this area of the code in the debugger, we can place a breakpoint in the printf() function with the command "BPX printf" (x32dbg).

It can be observed that shortly after pressing a key, a conditional jump (heh) is performed, which is always fulfilled. If it is not fulfilled, what seems to be the decryption of a text string is performed for its later display in a printf()

![](images/2.png)

If we try to patch this conditional jump so that it is not fulfilled (either by modifying it by a jne, or by not typing it), shortly after the program will end its execution process without giving any clue as to why this happens.

The first option that can occur to us is that it has a CRC, a code that is executed in another thread of execution that constantly checks the integrity of the program, verifying that no modification has been made in the instructions of this one. Therefore, we can proceed to place a read memory breakpoint in the .text section of the program and verify that this is really the case. Basically, if you are checking the integrity of the code, you are obliged to read it.

To do this, we go to the memory sections tab of our debugger and place a memory breakpoint in that section, as I do in x32dbg:

![](images/3.png)

When we run the program, eventually the breakpoint will interrupt a different thread and show it to us in the debugger:

![](images/4.png)

If we follow the execution thread, we will be able to observe that there is indeed a CRC checking the integrity of the conditional jump mentioned at the beginning of the writeup:

![](images/5.png)

It is checking the first two bytes (opcodes) of the instruction, 0x0F84, responsible for indicating that it is a JE (Jump if Equal). The other 4 bytes of the instruction only dictate the length of the jump (those 4 bytes are an int. It will jump that number of bytes plus another 5 that add all the jumps).

Therefore, if we get rid of this check and, subsequently, patch the first conditional jump of the writeup, we will be able to execute the part that decrypts the flag and displays it on the screen.

CRC patch:

![](images/6.png)

Patch to the first conditional jump:

![](images/7.png)

Flag:

![](images/8.png)

An alternative way to perform the whole process, skipping everything related to CRC evasion is to modify the registers before the first conditional jump that results in the display of the flag. In this case, eax would have to be modified. It is an unintentional way to solve the challenge but totally valid.

## Notes

None

## References

None

## Author

@nt_tnt
