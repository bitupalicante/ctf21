# Native

## Index

[[_TOC_]]

## Points

* Maximum Score `150`
* Minimum Score `50`
* Decadence `15`

## Hints

None

## Flag

`bitup21{c_plus_plus_aint_that_bad}`

## Attachments

* native.zip (MD5: 1389fa73b0450d5a89ec1d4d4241602d)
    * native.apk (MD5: d95fb9ed87b8c521f6e23b528d0bd0c2)

## Deploy

None

## Requirements

None

## Description

I keep my secrets natively, do you think you will be able to discover them?

## Writeup

1. Decompile the APK I'm using JADX to do that.
2. Navigate to `com.congon4tor.anative/MainActivity`. We can see the app imports a function from a native library.

```java
    static {
        System.loadLibrary("anative");
    }

    public native String decrypt(String str);
```

This function is called onclick of the button and passed the pin you input in the edittext.

3. Extract the library from Resources/lib/x86/libnative.so. You can open it in Ghidra or radare and reverse engineer it.

4. In ghidra look for the function `Java_com_congon4tor_anative_MainActivity_decrypt`.

5. The function makes reference to 2 pointers to char arrays lets investigate the first one.

```c++
__s1 = *(char **)((long)&PTR_s_01100010_00135b70 + lVar7);
```

It starts at 00135b70 with the value "01100010" which is ascii for `b`. By following the pointer to the actual address we find the following part of memory (as shown by ghidra)

```
                             s_1100010_0012c062                              XREF[3,7]:   Java_com_congon4tor_anative_Main
                             s_100010_0012c063                                            00135b70(*), 00135c60(*),
                             s_00010_0012c064                                             Java_com_congon4tor_anative_Main
                             s_0010_0012c065                                              Java_com_congon4tor_anative_Main
                             s_010_0012c066                                               Java_com_congon4tor_anative_Main
                             s_10_0012c067                                                Java_com_congon4tor_anative_Main
                             s_0_0012c068                                                 Java_com_congon4tor_anative_Main
                             s_01100010_0012c061                                          Java_com_congon4tor_anative_Main
                                                                                          Java_com_congon4tor_anative_Main
        0012c061 30 31 31        ds         "01100010"
                 30 30 30
                 31 30 00
                             DAT_0012c06a                                    XREF[3]:     Java_com_congon4tor_anative_Main
                                                                                          00135b78(*), 00135c18(*)
        0012c06a 30              undefined1 30h
                             DAT_0012c06b                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c06b 31              undefined1 31h
                             DAT_0012c06c                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c06c 31              undefined1 31h
                             DAT_0012c06d                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c06d 30              undefined1 30h
                             DAT_0012c06e                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c06e 31              undefined1 31h
                             DAT_0012c06f                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c06f 30              undefined1 30h
                             DAT_0012c070                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c070 30              undefined1 30h
                             DAT_0012c071                                    XREF[1]:     Java_com_congon4tor_anative_Main
        0012c071 31              undefined1 31h
        0012c072 00              ??         00h
                             s_01110100_0012c073                             XREF[4]:     00135b80(*), 00135c28(*),
                                                                                          00135c38(*), 00135c50(*)
        0012c073 30 31 31        ds         "01110100"
                 31 30 31
                 30 30 00
                             s_01110101_0012c07c                             XREF[3]:     00135b88(*), 00135bd0(*),
                                                                                          00135bf8(*)
        0012c07c 30 31 31        ds         "01110101"
                 31 30 31
                 30 31 00

```

I truncated it so it does not get too long. What we see here is that the first char array the binary for `b` is referenced by 00135b70 (which we already knew) and 00135c60. So there are more than one `b` in this.
Then we have another one which for some reason gets displayed slightly differently. `30` in the ascii table is a 0 and `31` is a 1 so we are looking at the char array 01101001 which is an i, this one is referenced by 00135b78 and 00135c18.

7. The access to these variables happens in a do while loop and we can see it does it until `lvar7` is equal to `0x110` which is 272 in decimal. However it is incrementing 8 at a time so 34 iterations.

```c++
    lVar7 = lVar7 + 8;
  } while (lVar7 != 0x110);
```

8. Starting at 00135b70 we lookup the value and convert the char array of 0s and 1s to its ascii character until we have 34 characters

```
135b70          b
135b78          i
135b80          t
135b88          u
135b90          p
135b98          2
135ba0          1
135ba8          {
135bb0          c
135bb8          _
135bc0          p
135bc8          l
135bd0          u
135bd8          s
135be0          _
135be8          p
135bf0          l
135bf8          u
135c00          s
135c08          _
135c10          a
135c18          i
135c20          n
135c28          t
135c30          _
135c38          t
135c40          h
135c48          a
135c50          t
135c58          _
135c60          b
135c68          a
135c70          d
135c78          }
```

And that is the flag `bitup21{c_plus_plus_aint_that_bad}`

## Notes

None

## References

None

## Author

@Congon4tor
