from flask import Flask, request
import json
import datetime
import os

app = Flask(__name__)

@app.route('/')
def index():
    for cookie in request.cookies:
        if request.cookies.get("protected") == "d41d8cd98f00b204e9800998ecf8427e":
            return os.getenv('FLAG')
    
    return "No flag sorry"
