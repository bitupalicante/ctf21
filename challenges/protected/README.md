# Protected

## Index

[[_TOC_]]

## Points

* Difficulty: Medium-Hard
* Maximum Score `400`
* Minimum Score `50`
* Decadence `15`

## Hints

None

## Flag

`bitup21{cheap_protection_wont_cut_it}`

## Attachments

* protected.zip (MD5: 3403e2f7c0d167fcafbd145521d563b4)
    * protected.apk (MD5: 318a99d52c4d041ca4a7b9f664d0c363)

## Deploy

Click [here](deploy) to see how to deploy the challenge with Docker. 

## Requirements

**Docker** is required to deploy the challenge.

## Description

The "native keep" didn't go very well, now I keep them protected, even so, do you think you will be able to discover them?

## Writeup

1. Decompile the APK using JADX.
2. Navigate to `com/congon4tor.aprotected/FlagActivity`
3. We can see the app does a HTTP request to `http://protected.bitupalicante.com` with the cookie `protected=d41d8cd98f00b204e9800998ecf8427e`.

4. `curl -b "protected=d41d8cd98f00b204e9800998ecf8427e" "http://protected.bitupalicante.com"`will return the flag

`bitup21{cheap_protection_wont_cut_it}`


## Notes

None

## References

None

## Author

@Congon4tor
