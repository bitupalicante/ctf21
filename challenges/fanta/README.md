# Fanta

## Index

[[_TOC_]]

## Points

* Difficulty: Hard

## Hints

None

## Flag

Each of the flags are distributed in the answers to the following questions:

1. When did the attacker first access the system? Is it possible to determine which protocol was used? Which was the IP used by the attacker?
    1. Format: `DATETIME (YY"-"MM"-"DD"T"HH:MM),PROTOCOL,IP`. Example: `2021-10-19T20:40,SMB,31.220.127.11`
    2. Flag: `2021-10-17T14:49,RDP,217.138.209.76`
    3. Points:
        1. Maximum Score: `100`
        2. Minimum Score `20`
        3. Decadence `5`

2. Was any persistence mechanism establish during the attack?
    1. Format: `REGISTRY`, `FILEPATH`, `USER`, etc. Example: could be `HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run`, `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup`, `MACHINE\LocalUser`, etc.
    2. Flag: `SRV01\localadmin`
    3. Points:
        1. Maximum Score: `75`
        2. Minimum Score `15`
        3. Decadence `5`

3. How much information was leaked during the attack?
    1. Format: `FLOAT(Gb|Mb|Kb|b)`. Example:  
    2. Flags:
        1. `0,570912485Gb`
        2. `570,912485Mb`
        3. `570912,485Kb`
        4. `570912485.0b`
        5. `570912485.0`
    3. Points:
        1. Maximum Score: `50`
        2. Minimum Score `10`
        3. Decadence `5`

4. Was any other user compromised during the attack?
    1. Format: `MACHINE\USER` or `DOMAIN\USER`. Example: `PC02\targetuser` or `LAB\targetuser`
    2. Flag: `BITUP21\Administrator`
    3. Points:
        1. Maximum Score: `75`
        2. Minimum Score `15`
        3. Decadence `5`

5. Did the attacker perform a lateral movement to another machine?
    1. Format: `REMOTE HOST,PROTOCOL,BINARY`. Example: `PC02,SMB,PsExec.exe`
    2. Flag: `DC01,RDP,MSTSC.exe`
    3. Points:
        1. Maximum Score: `75`
        2. Minimum Score `15`
        3. Decadence `5`

6. When was the ransomware executed?
    1. Format: `DATETIME (YY"-"MM"-"DD"T"HH:MM),PROTOCOL,REMOTE HOSTNAME,BINARY`. Example: `2021-10-19T20:40,SMB,PC02,ransom.exe`
    2. Flag: `2021-10-17T19:08,SMB,DC01,earnmoney.exe`
    3. Points:
        1. Maximum Score: `75`
        2. Minimum Score `15`
        3. Decadence `5`

7. How many files were encrypted by the ransomware?
    1. Format: `INTEGER`. Example: `1000`
    2. Flag: `216`
    3. Points:
        1. Maximum Score: `50`
        2. Minimum Score `10`
        3. Decadence `5`

## Attachments

* https://mega.nz/file/IRFESRxR#Kpe_Xc078oxb9-qWoF7kSOXG0w-1n2gwMsM4f2Fv5kk (Complete File, 4.43Gb, MD5:14494429aee471b9387c73ecc680fecf)
* https://drive.google.com/drive/folders/1SbPxw8b8vw7s_KVbByeGzIPvBz1JDjNX?usp=sharing (Complete File, 4.43Gb, MD5:14494429aee471b9387c73ecc680fecf)
  * $Extend.7z (MD5: fd359013813b13c2668823a8ec026eff)
  * $LogFile_c.7z (MD5: d5b6841b3dae54e391d0f92bcc38b6fb)
  * $MFT_c.7z (MD5: 2ce00f28335a125cc30542a951ffca62)
  * $Recycle.7z (MD5: b3afaeb681db77cca43f60eed42d5347)
  * Usuarios.7z (MD5: 045b551a36974243c7a8b7ed16675faa)
  * Windows.7z (MD5: 966b61a7802d05b59dbddeaf83a7d74a)

## Deploy

None

## Requirements

None

## Description

Charlie was a worker whose Monday couldn't have started any worse... Everything seemed to be going pretty well when he left on Friday afternoon, but as soon as he logs on his computer he notices that there's a new directory on his Desktop named "fanta". When he opens it he sees some very phisy binaries that are not familiar to him. Then, a panic attack arises once he opens the file named `ntr42-readme.txt` and he reads the following:

```
[+] Whats Happen? [+]

Your files are encrypted, and currently unavailable. You can check it: all files on you computer has expansion ntr42.
By the way, everything is possible to recover (restore), but you need to fo llow our instructions. Otherwise, you cant return your data (NEVER).
```

And it goes on... and it only gets worse.

Who could've imagined that having a service exposed to the Internet was a bad idea?

* Every answer is a flag:
  * **Question 1:** When did the attacker first access the system? Is it possible to determine which protocol was used? Which was the IP used by the attacker?
  * **Question 2:** Was any persistence mechanism establish during the attack?
  * **Question 3:** How much information was leaked during the attack?
  * **Question 4:** Was any other user compromised during the attack?
  * **Question 5:** Did the attacker perform a lateral movement to another machine?
  * **Question 6:** When was the ransomware executed?
  * **Question 7:** How many files were encrypted by the ransomware?


## Writeup

First things first, we need to decompress the 7z file to start the analysis.

### When did the attacker first access the system? Is it possible to determine which protocol was used? Which was the IP used by the attacker?


In order to be able to determine the first time the attacker logged on the machine, we can analyze several Windows event logs, among them: `Security` and `Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational`.

Several tools can be used to parse these logs. In this case, `evtx_dump` was used, and the command line used to parse the logs follows the next syntax: 

`evtx_dump --dont-show-record-number -o json -f "PATHTODESTINATIONFILE" [input]`

This applies to every single Windows event log that is required to be parsed during this analysis.


Just by looking at the Security logs, we can see tons of events with ID 4625 happening on October 17, which may indicate a brute force attack. Or maybe just some peers on the Internet trying to log onto the computer. 

With the following command line, we can extract a table of the number of failed logins on the machine based on the IP address used. Filtering by the day in which the attack took place. 

`echo "IpAddress|Number of failed logins" ; echo "--|--"; cat Security.json | jq -c '.' | rg '"EventID":4625' | rg '"SystemTime":"2021-10-17' | sed 's/^.*\("IpAddress":"[^\"]*"\).*/\1/' | sed 's/"IpAddress"://' | tr -d '"' |  sort -r |uniq -c |  awk '{print $2 "|" $1}'`

And the following output is obtained:

IpAddress|Number of failed logins
--|--
94.232.47.180|851
94.232.43.62|73
94.232.42.95|64
92.38.172.24|6974
87.251.67.97|441
87.251.64.20|1171
87.251.64.18|4
80.82.77.234|21
5.248.43.201|90
45.141.84.54|300
45.135.232.114|507
217.138.209.76|4990
212.102.35.157|11
187.195.104.47|32
185.176.222.106|8
181.214.206.220|32
181.214.206.199|53
170.239.54.248|26
141.98.81.125|159
127.0.0.1|1
122.186.23.243|34
110.15.51.203|110
-|3


Now, we should be able to see if this attack was successful by finding a 4624 event with these IP addresses on the Security event log with the following command line:


`echo "SystemTime|Computer|TargetUserName|IpAddress|LogonType"  ; echo "--|--|--|--|--"; cat Security.json | jq -c '.' | rg '"EventID":4624' | rg '"SystemTime":"2021-10-17' | grep '94\.232\.47\.180\|94\.232\.43\.62\|94\.232\.42\.95\|92\.38\.172\.24\|87\.251\.67\.97\|87\.251\.64\.20\|87\.251\.64\.18\|80\.82\.77\.234\|5\.248\.43\.201\|45\.141\.84\.54\|45\.135\.232\.114\|217\.138\.209\.76\|212\.102\.35\.157\|187\.195\.104\.47\|185\.176\.222\.106\|181\.214\.206\.220\|181\.214\.206\.199\|170\.239\.54\.248\|141\.98\.81\.125\|127\.0\.0\.1\|122\.186\.23\.243\|110\.15\.51\.203' | sed 's/^.*\("IpAddress":"[^\"]*"\).*\("LogonType":[0-9]*\).*\("TargetUserName":"[^\"]*"\).*\("Computer":"[^\"]*"\).*\("SystemTime":"[^\"]*"\).*/\1|\2|\3|\4|\5/' | sed 's/"IpAddress"://' | sed 's/"LogonType"://' | sed 's/"TargetUserName"://' | sed 's/"Computer"://' | sed 's/"SystemTime"://' | tr -d '"' | awk -F "|" '{print $5,$4,$3,$1,$2}' | sed 's/ /\|/g'`


The following table is obtained as the output:

SystemTime|Computer|TargetUserName|IpAddress|LogonType
--|--|--|--|--
2021-10-17T10:20:50.685725Z|SRV01.bitup21.internal|srv01admin|127.0.0.1|2
2021-10-17T10:20:50.685754Z|SRV01.bitup21.internal|srv01admin|127.0.0.1|2
2021-10-17T13:07:39.673498Z|SRV01.bitup21.internal|srv01admin|127.0.0.1|2
2021-10-17T13:07:39.673549Z|SRV01.bitup21.internal|srv01admin|127.0.0.1|2
2021-10-17T14:49:44.844645Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|3
2021-10-17T14:49:48.245143Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T14:49:48.245172Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T15:19:00.756509Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|3
2021-10-17T15:19:25.199308Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T15:19:25.199342Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T15:27:42.702287Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|3
2021-10-17T15:27:45.931685Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T15:27:45.931711Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T16:20:33.304432Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|3
2021-10-17T16:20:40.741558Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T16:20:40.741585Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T16:26:50.651181Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|3
2021-10-17T16:26:55.087889Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10
2021-10-17T16:26:55.087917Z|SRV01.bitup21.internal|srv01admin|217.138.209.76|10


And as we can see, the attack was successful using IP address `217.138.209.76`. There we have it, and its LogonType (10) indicates the connection was carried through remote desktop services.

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "AuthenticationPackageName": "Negotiate",
      "ElevatedToken": "%%1843",
      "ImpersonationLevel": "%%1833",
      "IpAddress": "217.138.209.76",
      "IpPort": "0",
      "KeyLength": 0,
      "LmPackageName": "-",
      "LogonGuid": "00000000-0000-0000-0000-000000000000",
      "LogonProcessName": "User32 ",
      "LogonType": 10,
      "ProcessId": "0x484",
      "ProcessName": "C:\\Windows\\System32\\svchost.exe",
      "RestrictedAdminMode": "%%1843",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x3e7",
      "SubjectUserName": "SRV01$",
      "SubjectUserSid": "S-1-5-18",
      "TargetDomainName": "BITUP21",
      "TargetLinkedLogonId": "0x160104",
      "TargetLogonId": "0x160166",
      "TargetOutboundDomainName": "-",
      "TargetOutboundUserName": "-",
      "TargetUserName": "srv01admin",
      "TargetUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TransmittedServices": "-",
      "VirtualAccount": "%%1843",
      "WorkstationName": "SRV01"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "5A08274B-C360-0000-7427-085A60C3D701"
        }
      },
      "EventID": 4624,
      "EventRecordID": 11551,
      "Execution": {
        "#attributes": {
          "ProcessID": 528,
          "ThreadID": 3616
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 12544,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T14:49:48.245172Z"
        }
      },
      "Version": 2
    }
  }
}
```

We can now double-check the first time the attacker logged on the computer by checking the `Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational` event log. In order to determine the first time the attacker accessed through RDP by looking at events with ID 21 (for new connections) and 25 (for reconnections of previously opened sessions)

As for the new connections, the following command line allows us to list the connections established with the IP address found in the Security logs.

`echo "SystemTime|Computer|User|IpAddress|SessionID"; echo "--|--|--|--|--"; cat LocalSession.json | jq -c '.' | rg '"EventID":21' | rg '217\.138\.209\.76' | sed 's/^.*\("Computer":"[^\"]*"\).*\("SystemTime":"[^\"]*"\).*\("Address":"[^\"]*"\).*\("SessionID":[0-9]*\).*\("User":"[^\"].*\-*\\\\.*"\).*/\1|\2|\3|\4|\5/' | sed 's/"Computer"://' | sed 's/"SystemTime"://' | sed 's/"Address"://' | sed 's/"SessionID"://' | sed 's/"User"://' | tr -d '"' |  awk -F "|" '{print $2,$1,$5,$3,$4}' | sed 's/ /\|/g'`


SystemTime|Computer|User|IpAddress|SessionID
--|--|--|--|--
2021-10-17T14:49:49.480230Z|SRV01.bitup21.internal|BITUP21\\srv01admin|217.138.209.76|3


```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "System": {
      "Channel": "Microsoft-Windows-TerminalServices-LocalSessionManager/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "F420BD38-A73C-4D78-A6F9-1AAB1F0E0000"
        }
      },
      "EventID": 21,
      "EventRecordID": 259,
      "Execution": {
        "#attributes": {
          "ProcessID": 600,
          "ThreadID": 2184
        }
      },
      "Keywords": "0x1000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5D896912-022D-40AA-A3A8-4FA5515C76D7",
          "Name": "Microsoft-Windows-TerminalServices-LocalSessionManager"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 0,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T14:49:49.480230Z"
        }
      },
      "Version": 0
    },
    "UserData": {
      "EventXML": {
        "#attributes": {
          "xmlns": "Event_NS"
        },
        "Address": "217.138.209.76",
        "SessionID": 3,
        "User": "BITUP21\\srv01admin"
      }
    }
  }
}

```

These actions entail the first phase of this attack, which is **INITIAL ACCESS**:

![](images/initial_access.png)


Although this is not required throughout the questions laid out in this CTF, it is important to be aware of the time in which the attacker was logged into the system.

Several reconnections were found with IP address `217.138.209.76` with the following command line:

`echo "SystemTime|Computer|User|IpAddress|SessionID"; echo "--|--|--|--|--"; cat LocalSession.json | jq -c '.' | rg '"EventID":25' | rg '217\.138\.209\.76' | sed 's/^.*\("Computer":"[^\"]*"\).*\("SystemTime":"[^\"]*"\).*\("Address":"[^\"]*"\).*\("SessionID":[0-9]*\).*\("User":"[^\"].*\-*\\\\.*"\).*/\1|\2|\3|\4|\5/' | sed 's/"Computer"://' | sed 's/"SystemTime"://' | sed 's/"Address"://' | sed 's/"SessionID"://' | sed 's/"User"://' | tr -d '"' |  awk -F "|" '{print $2,$1,$5,$3,$4}' | sed 's/ /\|/g'`


SystemTime|Computer|User|IpAddress|SessionID
--|--|--|--|--
2021-10-17T15:19:26.079048Z|SRV01.bitup21.internal|BITUP21\\srv01admin|217.138.209.76|3
2021-10-17T15:27:46.570654Z|SRV01.bitup21.internal|BITUP21\\srv01admin|217.138.209.76|3
2021-10-17T16:20:41.236437Z|SRV01.bitup21.internal|BITUP21\\srv01admin|217.138.209.76|3
2021-10-17T16:26:55.724734Z|SRV01.bitup21.internal|BITUP21\\srv01admin|217.138.209.76|3



Finally, we can check when the attacker logged out with event ID 23 by correlating with the session ID, which is 3.

`echo "SystemTime|Computer|User|SessionID"; echo "--|--|--|--"; cat LocalSession.json | jq -c '.' | rg '"EventID":23' | rg '"SessionID":3' | sed 's/^.*\("Computer":"[^\"]*"\).*\("SystemTime":"[^\"]*"\).*\("SessionID":[0-9]*\).*\("User":"[^\"].*\-*\\\\.*"\).*/\1|\2|\3|\4/' | sed 's/"Computer"://' | sed 's/"SystemTime"://' | sed 's/"SessionID"://' | sed 's/"User"://' | tr -d '"' |  awk -F "|" '{print $2,$1,$4,$3}' | sed 's/ /\|/g'`


SystemTime|Computer|User|SessionID
--|--|--|--
2021-10-17T19:31:15.318038Z|SRV01.bitup21.internal|BITUP21\\srv01admin|3

To sum it up... Ok, cool, now we know that the first connection took place at **`2021-10-17T14:49:49`** with IP: **`217.138.209.76`** and user **`BITUP21\srv01admin`**


### Was any persistence mechanism established during the attack?

If we keep digging on the Security log events we can find events with ID 4720 and 4732, these indicate the creation of a new user on the machine, as well as its inclusion in the administrators' group on the machine.

To continue with this analysis, we'll need to parse the MFT. To do so, the tool `MFTECmd` by Eric Zimmermann will help by executing the following command:

`MFTECmd.exe -f $MFT --body "PATH_TO_OUTPUT_DIRECTORY" --bdl c`

Where:
* `-f`: File to process, in this case, the MFT
* `--body`: Output directory to save bodyfile to.
* `--bdl`: Drive letter to use in the bodyfile.
 
After the body file is created, in order to visualize with human-readable formats, we'll use the command `mactime` as follows:

`mactime -y -d -b 20211018103418_MFTECmd_\$MFT_Output.body > TL.csv`

Where:
* `-y`: Dates are displayed in ISO 8601 format
* `-d`: Output in comma delimited format
* `-b`: Specifies the body file location, else STDIN is used

This will allow us to create a more user-friendly timeline to analyze.

After checking the MFT entries around the same time these events take place... we can see that there's some modification and change in the MFT activity going on:

```csv
2021-10-17T15:17:15Z,5316,m.c.,r/rrwxrwxrwx,0,0,116208-128-4,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.tzfQR7GC.20211017165538.txt"
```

After checking the file `c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.tzfQR7GC.20211017165538.txt` we find that the attacker created the user with the following command line:

```
New-LocalUser -AccountNeverExpires:$true -Password ( ConvertTo-SecureString -AsPlainText -Force 'P@ssw0rd!123') -Name 'localadmin' | Add-LocalGroupMember -Group Administradores
```

Consequently, the following event is generated with the creation of the user **`localadmin`**

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "AccountExpires": "%%1794",
      "AllowedToDelegateTo": "-",
      "DisplayName": "%%1793",
      "HomeDirectory": "%%1793",
      "HomePath": "%%1793",
      "LogonHours": "%%1797",
      "NewUacValue": "0x15",
      "OldUacValue": "0x0",
      "PasswordLastSet": "%%1794",
      "PrimaryGroupId": "513",
      "PrivilegeList": "-",
      "ProfilePath": "%%1793",
      "SamAccountName": "localadmin",
      "ScriptPath": "%%1793",
      "SidHistory": "-",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x160104",
      "SubjectUserName": "srv01admin",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TargetDomainName": "SRV01",
      "TargetSid": "S-1-5-21-1618083873-720614790-33425247-1000",
      "TargetUserName": "localadmin",
      "UserAccountControl": "\r\n\t\t%%2080\r\n\t\t%%2082\r\n\t\t%%2084",
      "UserParameters": "%%1793",
      "UserPrincipalName": "-",
      "UserWorkstations": "%%1793"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "5A08274B-C360-0000-7427-085A60C3D701"
        }
      },
      "EventID": 4720,
      "EventRecordID": 12866,
      "Execution": {
        "#attributes": {
          "ProcessID": 528,
          "ThreadID": 3616
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13824,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:17:14.079249Z"
        }
      },
      "Version": 0
    }
  }
}
```

In the following event we can see a change in the group `Administradores` on the machine as a result of the execution of the command line detailed above:

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "MemberName": "-",
      "MemberSid": "S-1-5-21-1618083873-720614790-33425247-1000",
      "PrivilegeList": "-",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x160104",
      "SubjectUserName": "srv01admin",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TargetDomainName": "Builtin",
      "TargetSid": "S-1-5-32-544",
      "TargetUserName": "Administradores"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "5A08274B-C360-0000-7427-085A60C3D701"
        }
      },
      "EventID": 4732,
      "EventRecordID": 12872,
      "Execution": {
        "#attributes": {
          "ProcessID": 528,
          "ThreadID": 3616
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13826,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:17:14.124182Z"
        }
      },
      "Version": 0
    }
  }
}
```


This allows the attacker access to the machine, even if the compromised user's (`srv01admin`) credentials change, so it can be defined as a **persistence mechanism**.


These actions belong to the second phase of this attack: **ESTABLISHMENT OF PERSISTENCE MECHANISMS**

![](images/persistence_mechanism.png)

### How much information was leaked during the attack? What was the name of the leaked file?

Several artifacts can provide us with activity regarding file creation, such as MFT, LNK files, RecentFiles, or Sysmon events. And, especially, the creation of compressed files may indicate a feasible leak of information.

By taking a peek at the MFT... It kinda looks like a zip file was sent to the recycle bin and the Databases directory was accessed:

```csv
2021-10-17T15:42:10Z,570912485,.a.b,r/rrwxrwxrwx,0,0,116377-128-3,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip"
2021-10-17T15:42:10Z,570912485,.a.b,r/rrwxrwxrwx,0,0,116377-48-7,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip ($FILE_NAME)"
2021-10-17T15:42:27Z,770,m.c.,r/rrwxrwxrwx,0,0,116452-128-4,"c:/Users/srv01admin/AppData/Roaming/Microsoft/Windows/PowerShell/PSReadline/ConsoleHost_history.txt"
2021-10-17T15:42:39Z,570912485,m...,r/rrwxrwxrwx,0,0,116377-128-3,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip"
2021-10-17T15:42:39Z,570912485,m...,r/rrwxrwxrwx,0,0,116377-48-7,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip ($FILE_NAME)"
2021-10-17T15:44:29Z,887,macb,r/rrwxrwxrwx,0,0,116364-128-4,"c:/Users/srv01admin/AppData/Roaming/Microsoft/Windows/Recent/Databases.lnk"
2021-10-17T15:44:29Z,887,macb,r/rrwxrwxrwx,0,0,116364-48-2,"c:/Users/srv01admin/AppData/Roaming/Microsoft/Windows/Recent/Databases.lnk ($FILE_NAME)"
2021-10-17T15:44:29Z,570912485,..c.,r/rrwxrwxrwx,0,0,116377-48-7,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip ($FILE_NAME)"
.
.
.
2021-10-17T15:55:48Z,570912485,..c.,r/rrwxrwxrwx,0,0,116377-128-3,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$R4JE4IG.zip"
2021-10-17T15:55:48Z,122,macb,r/rrwxrwxrwx,0,0,116390-128-1,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$I4JE4IG.zip"
2021-10-17T15:55:48Z,122,macb,r/rrwxrwxrwx,0,0,116390-48-2,"c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108/$I4JE4IG.zip ($FILE_NAME)"
```

We can now only rely on $R and $I files to determine the size and name of it. 

We can double-check the size of the zip file with the $R file, located in `c:/$Recycle.Bin/S-1-5-21-3541119100-3653002476-1256659928-1108`, which is **`570912485`** bytes.

However, the name of it will be obtained from parsing the $I file. And after parsing it with the tool `$I Parse` by Digital Forensics Stream, we get that the zip created was named **`Databases.zip`** and it was located on the directory `C:\Users\srv01admin\Confidential`.

These actions belong to the third phase of this attack: **DATA LEAK**

![](images/data_leak.png)

### Was any other user compromised during the attack? If so, provide its full username.

By looking through the program executions on the machine we can find some very fishy tools... artifacts such as Amcache.hve, MFT, or Windows event logs may show us the executions that took place during the attack. Unluckily for us, the Windows Registry, parsed with `regripper` hasn't allowed us to determine the executions that took place during the attack.

With the analysis of the Sysmon logs, we can see the creation of both binaries `Rubeus.exe` and `mimikatz.exe` with events of ID 11 once the attacker dropped them in the directory `C:\Users\srv01admin\Desktop\fanta`.


```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CreationUtcTime": "2021-10-17 15:36:12.307",
      "Image": "C:\\Windows\\explorer.exe",
      "ProcessGuid": "D5D3D798-41A5-616C-D500-000000001200",
      "ProcessId": 5604,
      "RuleName": "EXE",
      "TargetFilename": "C:\\Users\\srv01admin\\Desktop\\fanta\\Rubeus.exe",
      "UtcTime": "2021-10-17 15:36:12.307"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 11,
      "EventRecordID": 18342,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 11,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:36:12.309862Z"
        }
      },
      "Version": 2
    }
  }
}
```

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CreationUtcTime": "2021-10-17 15:05:00.385",
      "Image": "C:\\Windows\\Explorer.EXE",
      "ProcessGuid": "D5D3D798-380E-616C-7900-000000001200",
      "ProcessId": 2652,
      "RuleName": "EXE",
      "TargetFilename": "C:\\Users\\srv01admin\\Desktop\\fanta\\mimikatz.exe",
      "UtcTime": "2021-10-17 15:05:00.385"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 11,
      "EventRecordID": 16529,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 11,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:05:00.398951Z"
        }
      },
      "Version": 2
    }
  }
}
```

Later, the execution of both these binaries can be seen with events in the Security (4688) and Sysmon (1) logs:


```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"C:\\Users\\srv01admin\\Desktop\\fanta\\Rubeus.exe\" dump",
      "Company": "-",
      "CurrentDirectory": "C:\\Users\\srv01admin\\Desktop\\fanta\\",
      "Description": "Rubeus",
      "FileVersion": "1.0.0.0",
      "Hashes": "MD5=6833A0F72A85099EB089894C24A95B84,SHA256=0B36F5BA1F08862FB413205CD2A2F00A1713F57B11D9609B9EB4BC8BF86768A6,IMPHASH=00000000000000000000000000000000",
      "Image": "C:\\Users\\srv01admin\\Desktop\\fanta\\Rubeus.exe",
      "IntegrityLevel": "High",
      "LogonGuid": "D5D3D798-380C-616C-0401-160000000000",
      "LogonId": "0x160104",
      "OriginalFileName": "Rubeus.exe",
      "ParentCommandLine": "\"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" ",
      "ParentImage": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
      "ParentProcessGuid": "D5D3D798-4340-616C-E400-000000001200",
      "ParentProcessId": 2444,
      "ProcessGuid": "D5D3D798-4370-616C-E600-000000001200",
      "ProcessId": 3836,
      "Product": "Rubeus",
      "RuleName": "-",
      "TerminalSessionId": 3,
      "User": "BITUP21\\srv01admin",
      "UtcTime": "2021-10-17 15:38:24.204"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 1,
      "EventRecordID": 18504,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 1,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:38:24.220543Z"
        }
      },
      "Version": 5
    }
  }
}
```

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"C:\\Users\\srv01admin\\Desktop\\fanta\\Rubeus.exe\" dump",
      "MandatoryLabel": "S-1-16-12288",
      "NewProcessId": "0xefc",
      "NewProcessName": "C:\\Users\\srv01admin\\Desktop\\fanta\\Rubeus.exe",
      "ParentProcessName": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
      "ProcessId": "0x98c",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x160104",
      "SubjectUserName": "srv01admin",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TargetDomainName": "-",
      "TargetLogonId": "0x0",
      "TargetUserName": "-",
      "TargetUserSid": "S-1-0-0",
      "TokenElevationType": "%%1937"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 4688,
      "EventRecordID": 14609,
      "Execution": {
        "#attributes": {
          "ProcessID": 4,
          "ThreadID": 4560
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13312,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:38:24.205105Z"
        }
      },
      "Version": 2
    }
  }
}
```


```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"C:\\Users\\srv01admin\\Desktop\\fanta\\mimikatz.exe\"",
      "Company": "gentilkiwi (Benjamin DELPY)",
      "CurrentDirectory": "C:\\Users\\srv01admin\\Desktop\\fanta\\",
      "Description": "mimikatz for Windows",
      "FileVersion": "2.2.0.0",
      "Hashes": "MD5=BB8BDB3E8C92E97E2F63626BC3B254C4,SHA256=912018AB3C6B16B39EE84F17745FF0C80A33CEE241013EC35D0281E40C0658D9,IMPHASH=9528A0E91E28FBB88AD433FEABCA2456",
      "Image": "C:\\Users\\srv01admin\\Desktop\\fanta\\mimikatz.exe",
      "IntegrityLevel": "High",
      "LogonGuid": "D5D3D798-380C-616C-0401-160000000000",
      "LogonId": "0x160104",
      "OriginalFileName": "mimikatz.exe",
      "ParentCommandLine": "\"C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" ",
      "ParentImage": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
      "ParentProcessGuid": "D5D3D798-4340-616C-E400-000000001200",
      "ParentProcessId": 2444,
      "ProcessGuid": "D5D3D798-4463-616C-0001-000000001200",
      "ProcessId": 3556,
      "Product": "mimikatz",
      "RuleName": "-",
      "TerminalSessionId": 3,
      "User": "BITUP21\\srv01admin",
      "UtcTime": "2021-10-17 15:42:27.904"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 1,
      "EventRecordID": 18863,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 1,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:42:27.943770Z"
        }
      },
      "Version": 5
    }
  }
}
```


```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"C:\\Users\\srv01admin\\Desktop\\fanta\\mimikatz.exe\"",
      "MandatoryLabel": "S-1-16-12288",
      "NewProcessId": "0xde4",
      "NewProcessName": "C:\\Users\\srv01admin\\Desktop\\fanta\\mimikatz.exe",
      "ParentProcessName": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
      "ProcessId": "0x98c",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x160104",
      "SubjectUserName": "srv01admin",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TargetDomainName": "-",
      "TargetLogonId": "0x0",
      "TargetUserName": "-",
      "TargetUserSid": "S-1-0-0",
      "TokenElevationType": "%%1937"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 4688,
      "EventRecordID": 14928,
      "Execution": {
        "#attributes": {
          "ProcessID": 4,
          "ThreadID": 260
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13312,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T15:42:27.904568Z"
        }
      },
      "Version": 2
    }
  }
}
```

So... if we look at the MFT entries at the time `Rubeus.exe` was executed we can find the following:


```csv
2021-10-17T15:36:12Z,277504,.a.b,r/rrwxrwxrwx,0,0,116519-128-1,"c:/Users/srv01admin/Desktop/fanta/Rubeus.exe"
2021-10-17T15:36:12Z,277504,macb,r/rrwxrwxrwx,0,0,116519-48-2,"c:/Users/srv01admin/Desktop/fanta/Rubeus.exe ($FILE_NAME)"
2021-10-17T15:37:21Z,747,.a.b,r/rrwxrwxrwx,0,0,116206-128-4,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.KRY+l68T.20211017173721.txt"
2021-10-17T15:37:21Z,747,macb,r/rrwxrwxrwx,0,0,116206-48-2,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.KRY+l68T.20211017173721.txt ($FILE_NAME)"
2021-10-17T15:37:24Z,747,m.c.,r/rrwxrwxrwx,0,0,116206-128-4,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.KRY+l68T.20211017173721.txt"
2021-10-17T15:37:36Z,29222,.a.b,r/rrwxrwxrwx,0,0,116520-128-4,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.u94ql02i.20211017173736.txt"
2021-10-17T15:37:36Z,29222,macb,r/rrwxrwxrwx,0,0,116520-48-2,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.u94ql02i.20211017173736.txt ($FILE_NAME)"
2021-10-17T15:37:36Z,5454,mac.,r/rrwxrwxrwx,0,0,116522-128-4,"c:/Users/srv01admin/AppData/Roaming/Microsoft/Windows/Recent/CustomDestinations/590aee7bdd69b59b.customDestinations-ms"
2021-10-17T15:37:36Z,5454,mac.,r/rrwxrwxrwx,0,0,116522-48-5,"c:/Users/srv01admin/AppData/Roaming/Microsoft/Windows/Recent/CustomDestinations/590aee7bdd69b59b.customDestinations-ms ($FILE_NAME)"
2021-10-17T15:38:25Z,29222,m.c.,r/rrwxrwxrwx,0,0,116520-128-4,"c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.u94ql02i.20211017173736.txt"
```

Which, again, indicates that the output of the execution was written to the file `c:/Windows/Temp/PowerShell_T/20211017/PowerShell_transcript.SRV01.u94ql02i.20211017173736.txt`.

That contains the dump of tickets, which would've allowed the attacker to perform a Pass-The-Ticket attack. However, there's no trace of this attack being performed on the machine.

According to the transcript, the credentials of the user `Administrator`, which is the domain admin, were used in order to log into this computer via RDP.

```
Action: Dump Kerberos Ticket Data (All Users)

[*] Current LUID    : 0x160104

  UserName                 : Administrador
  Domain                   : BITUP21
  LogonId                  : 0x7b4e4
  UserSID                  : S-1-5-21-3541119100-3653002476-1256659928-500
  AuthenticationPackage    : Kerberos
  LogonType                : RemoteInteractive
  LogonTime                : 17/10/2021 16:21:34
  LogonServer              : DC01
  LogonServerDNSDomain     : BITUP21.INTERNAL
  UserPrincipalName        : Administrador@BITUP21.INTERNAL


    ServiceName           :  krbtgt/BITUP21.INTERNAL
    ServiceRealm          :  BITUP21.INTERNAL
    UserName              :  Administrador
    UserRealm             :  BITUP21.INTERNAL
    StartTime             :  17/10/2021 16:21:50
    EndTime               :  18/10/2021 2:21:50
    RenewTill             :  24/10/2021 16:21:50
    Flags                 :  name_canonicalize, pre_authent, initial, renewable, forwardable
    KeyType               :  aes256_cts_hmac_sha1
    Base64(key)           :  zpjU+CzLF1OyDqUPuC+QpmYmgNeblLcwKuWj+cYdYwE=
    Base64EncodedTicket   :

      doIFhDCCBYCgAwIBBaEDAgEWooIEeDCCBHRhggRwMIIEbKADAgEFoRIbEEJJVFVQMjEuSU5URVJOQUyiJTAjoAMCAQKhHDAaGwZr
      cmJ0Z3QbEEJJVFVQMjEuSU5URVJOQUyjggQoMIIEJKADAgESoQMCAQKiggQWBIIEEsfUYq+EtoSHgAQg4pE6a9/QQ3DQGgMNlUv0
      XN6D4cJ0Ke0cxuCBV4XTN2TjlTZl+gChyG/ebMXrwkp6HFkPAJowS7gGm/YOIfPP5PaAxMnxgNf0W2G8F7yANMYGhg4fIeKQy0xM
      c5qf2Fr4rnwEEoPWIjtkhgpXIAkgAbs7Rvjn5pw6BTssgNQAFMtCQ3XbmQ+b+AYrn1O+lrDNfnAtA9JFOU1TYQfoYN36GG+xEYS7
      d/91+zYu9+Z3MhGkv4KB5DjoH5FTTlY746Wu0khEh/rlicAsiCQZwrKaJklIKz03UA+NJLOnqKryHyMqrMtzfmaLvTxvU2Z1d93d
      NzEKT7GQH1BqNKjfFIO/LVkRFV4QjwlfNui8cxRg4K7aatO1zcwz8kpA58e0qbM0X0lvnehtE2OavlSZjBgL9jfqtzD566ZP8j3a
      QOyI+EsFHjPCJEBJ+Bh8E6wgzVbrr1giS0tWkgj9nV4OA1KP9GDGYhUqsBfSsy23jPQWJ5O0r99WnTblQTziB171JuP4wUifgM1g
      P5jxFIXs2Cv2XMJBW7LhnDPxnxjv9MOi/Qr6+J/3FTjHL97QRh+a9khtXwxAAJrgjfPwU6adGVONRqUknwuAd+vOVM2W/pCJHLOg
      zgn+My9FCQsVJ1sLf+CjgaffMRC8zYRvqu3ivSjFtUf7fQv485/5SUH6CDMA25gDl1eVYUGj8V9pGwdEY6pcCL8e4Lv4hKYQWlj9
      VC+w6uLlm60/ru0r7ZqIHeOqJRxp7QucgOEVeZWP3DaKNO0JygL3Xbcl1rVPjGbeouVc/pNy5tVb7y48OMMmym/c7skAzewHtb+1
      tc/XWFsTKVBFDQTrPK/sLdi5vTJylaspA27cLfPj5bqMgnLe8L6oT+Yp8U8F6Me5ZkJGF0TxmXBF7DUtWaaZ9tqn3IgJn8/cJqVn
      S+GmVa9Ar1qWabQqf8tziZ+NKTew9u6XYfD8b+BDnmpOPoVKsY75wKRG5bBguy/L96VG2HJDGSaaZRnw3QZC6qLZECsLSIXAYTre
      1aJpWFrH81IVw1sMuW56wRQAwqlsL52Vj0LQ8N5KaNtj0qUJ3qWBV/k6eAAnAUddyIr6wKCNSo6+FAnpB88ZB9LBuNsucqZz37eu
      YWSfeQY7FVnQXxo6WBNkPxHRZLRj4tEm+9rTv26+0cDxtRXzzQsM2AfBqA13EakkZsQpJ3vdMa2Jzpds+W/5yIekcnVuqLktLvNp
      SlqJGgE8oGo7T+bV3sTkp6iEn5QJrC+EjT6NVe9oBiY/ocC6ib7TicNPY7+c+/TrPP7VmDo3WCui2NZUrpgu++VGOP62VnXjrAzf
      0WEtjvCWw9gKntY51fsjRhYr4tyScwAmi5exFpRrG12paryYGjaUTM2jgfcwgfSgAwIBAKKB7ASB6X2B5jCB46CB4DCB3TCB2qAr
      MCmgAwIBEqEiBCDOmNT4LMsXU7IOpQ+4L5CmZiaA15uUtzAq5aP5xh1jAaESGxBCSVRVUDIxLklOVEVSTkFMohowGKADAgEBoREw
      DxsNQWRtaW5pc3RyYWRvcqMHAwUAQOEAAKURGA8yMDIxMTAxNzE0MjE1MFqmERgPMjAyMTEwMTgwMDIxNTBapxEYDzIwMjExMDI0
      MTQyMTUwWqgSGxBCSVRVUDIxLklOVEVSTkFMqSUwI6ADAgECoRwwGhsGa3JidGd0GxBCSVRVUDIxLklOVEVSTkFM

```

The credentials of the user could've been retrieved with the execution of `mimikatz.exe`. However, unluckily for us, its output was not saved in any of the transcripts. But, after checking the value of `HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\WDigest\UseLogonCredential`, with the tool RegistryExplorer we can see the following:

![](images/wdigest.png)

If this value was set to 1, we can affirm that the password of this user was presented in clear text after the execution of mimikatz. 


These actions entail the fourth phase of this attack, which is **POST-EXPLOITATION**:

![](images/post_exploitation.png)


However, if we keep digging on the Security (Event ID 4688) or Sysmon (Event ID 1) logs we can see that a runas command was used with user `BITUP21\Administrator`:

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "runas  /netonly /user:BITUP21\\Administrador cmd",
      "Company": "Microsoft Corporation",
      "CurrentDirectory": "C:\\Users\\srv01admin\\Desktop\\fanta\\",
      "Description": "Run As Utility",
      "FileVersion": "10.0.14393.0 (rs1_release.160715-1616)",
      "Hashes": "MD5=04A3526D77C0C4622517F6E848A3D1E2,SHA256=06DD3C38BF47D2FAAEDDEBC27C3A1EB1D329F0E8664E0D0308B06F6214DDCA96,IMPHASH=89758AD95FE7510ED40C5D4DD1BFE503",
      "Image": "C:\\Windows\\System32\\runas.exe",
      "IntegrityLevel": "High",
      "LogonGuid": "D5D3D798-380C-616C-0401-160000000000",
      "LogonId": "0x160104",
      "OriginalFileName": "RUNAS.EXE",
      "ParentCommandLine": "\"C:\\Windows\\system32\\cmd.exe\" ",
      "ParentImage": "C:\\Windows\\System32\\cmd.exe",
      "ParentProcessGuid": "D5D3D798-4B49-616C-3501-000000001200",
      "ParentProcessId": 5124,
      "ProcessGuid": "D5D3D798-4CA4-616C-3B01-000000001200",
      "ProcessId": 4836,
      "Product": "Microsoft® Windows® Operating System",
      "RuleName": "-",
      "TerminalSessionId": 3,
      "User": "BITUP21\\srv01admin",
      "UtcTime": "2021-10-17 16:17:40.869"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 1,
      "EventRecordID": 22243,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 1,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T16:17:40.885434Z"
        }
      },
      "Version": 5
    }
  }
}
```

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "runas  /netonly /user:BITUP21\\Administrador cmd",
      "MandatoryLabel": "S-1-16-12288",
      "NewProcessId": "0x12e4",
      "NewProcessName": "C:\\Windows\\System32\\runas.exe",
      "ParentProcessName": "C:\\Windows\\System32\\cmd.exe",
      "ProcessId": "0x1404",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x160104",
      "SubjectUserName": "srv01admin",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-1108",
      "TargetDomainName": "-",
      "TargetLogonId": "0x0",
      "TargetUserName": "-",
      "TargetUserSid": "S-1-0-0",
      "TokenElevationType": "%%1937"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 4688,
      "EventRecordID": 18273,
      "Execution": {
        "#attributes": {
          "ProcessID": 4,
          "ThreadID": 1744
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13312,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T16:17:40.869302Z"
        }
      },
      "Version": 2
    }
  }
}
```


This would've allowed the attacker to run commands con the Windows console as the user **`BITUP21\Administrador`**

### Did the attacker perform a lateral movement to another machine?

If we take a look at the tools dropped by the attacker we can assume that maybe a lateral movement was performed using PsExec for remote execution. However, there were no signs of PsExec execution in both Security and Sysmon logs, with user `srv01admin`. Therefore, the next option is looking at the event log `Microsoft-Windows-TerminalServices-RDPClient%4Operational`. And, we can find the following event with ID 1024. As we could imagine, the attacker performed a lateral movement to the DC via RDP, most likely using the domain admin's credentials.

`echo "SystemTime|Source Machine|Destination Machine"; echo "--|--|--"; cat RDPClient.json | jq -c '.' | rg '"EventID":1024' | rg '"SystemTime":"2021-10-17' | sed 's/^.*\("Value":"[^\"]*"\).*\("Computer":"[^\"]*"\).*\("SystemTime":"[^\"]*"\).*.*/\1|\2|\3/' | sed 's/"Computer"://' | sed 's/"SystemTime"://' | sed 's/"Value"://' | tr -d '"' |  awk -F "|" '{print $3,$2,$1}' | sed 's/ /\|/g'`

SystemTime|Source Machine|Destination Machine
--|--|--
2021-10-17T13:11:25.803573Z|SRV01.bitup21.internal|DC01
2021-10-17T16:00:09.749517Z|SRV01.bitup21.internal|dc01
2021-10-17T16:00:09.765736Z|SRV01.bitup21.internal|dc01
2021-10-17T16:27:48.113883Z|SRV01.bitup21.internal|dc01

Where the first connection that appears on the table above is not considered relevant for this analysis since the attacker was not logged onto the machine at that time.

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CustomLevel": "Info",
      "Name": "Server Name",
      "Value": "dc01"
    },
    "System": {
      "Channel": "Microsoft-Windows-TerminalServices-RDPClient/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "81599834-F5DC-4096-8D9F-F2D9A7150000"
        }
      },
      "EventID": 1024,
      "EventRecordID": 21,
      "Execution": {
        "#attributes": {
          "ProcessID": 4392,
          "ThreadID": 4460
        }
      },
      "Keywords": "0x4000000000000000",
      "Level": 4,
      "Opcode": 10,
      "Provider": {
        "#attributes": {
          "Guid": "28AA95BB-D444-4719-A36F-40462168127E",
          "Name": "Microsoft-Windows-TerminalServices-ClientActiveXCore"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-21-3541119100-3653002476-1256659928-1108"
        }
      },
      "Task": 101,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T16:00:09.749517Z"
        }
      },
      "Version": 0
    }
  }
}
```

Then, we can state that the attacker performed a lateral movement towards **`dc01`** via **`RDP`**.

This action constitutes the fifth phase of this attack, which is **LATERAL MOVEMENT**:

![](images/lateral_movement.png)

### When was the ransomware executed?

So it seems like the attacker chose to deploy the ransomware from the DC after performing the lateral movement... Security and Sysmon logs provide us with the answer again. Let's take a look at the events 4688 and 1 and... There it is, this is what PsExec was dropped for.

Several aspects to point out here, first we see the creation of the binary PSEXESVC.EXE on the machine:

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CreationUtcTime": "2021-10-17 19:08:11.762",
      "Image": "System",
      "ProcessGuid": "D5D3D798-2E2F-616C-EB03-000000000000",
      "ProcessId": 4,
      "RuleName": "EXE",
      "TargetFilename": "C:\\Windows\\PSEXESVC.exe",
      "UtcTime": "2021-10-17 19:08:11.762"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 11,
      "EventRecordID": 23113,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 11,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:11.774701Z"
        }
      },
      "Version": 2
    }
  }
}
```

As well as the service installation with events 4697 (Security) and 13 (Sysmon):

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "Details": "DWORD (0x00000003)",
      "EventType": "SetValue",
      "Image": "C:\\Windows\\system32\\services.exe",
      "ProcessGuid": "D5D3D798-2E32-616C-0A00-000000001200",
      "ProcessId": 520,
      "RuleName": "T1031,T1050",
      "TargetObject": "HKLM\\System\\CurrentControlSet\\Services\\PSEXESVC\\Start",
      "UtcTime": "2021-10-17 19:08:34.299"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 13,
      "EventRecordID": 23131,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 13,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:34.311505Z"
        }
      },
      "Version": 2
    }
  }
}
```

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "ServiceAccount": "LocalSystem",
      "ServiceFileName": "%SystemRoot%\\PSEXESVC.exe",
      "ServiceName": "PSEXESVC",
      "ServiceStartType": 3,
      "ServiceType": "0x10",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x5312d9",
      "SubjectUserName": "Administrador",
      "SubjectUserSid": "S-1-5-21-3541119100-3653002476-1256659928-500"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": {
        "#attributes": {
          "ActivityID": "5A08274B-C360-0000-7427-085A60C3D701"
        }
      },
      "EventID": 4697,
      "EventRecordID": 19236,
      "Execution": {
        "#attributes": {
          "ProcessID": 528,
          "ThreadID": 560
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 12289,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:34.311087Z"
        }
      },
      "Version": 0
    }
  }
}
```


After the installation of the service `PSEXESVC.EXE` is completed, its execution invokes consequently the execution of the binary that triggers the *ransomware* attack.

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "C:\\Windows\\PSEXESVC.exe",
      "MandatoryLabel": "S-1-16-16384",
      "NewProcessId": "0x82c",
      "NewProcessName": "C:\\Windows\\PSEXESVC.exe",
      "ParentProcessName": "C:\\Windows\\System32\\services.exe",
      "ProcessId": "0x208",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x3e7",
      "SubjectUserName": "SRV01$",
      "SubjectUserSid": "S-1-5-18",
      "TargetDomainName": "-",
      "TargetLogonId": "0x0",
      "TargetUserName": "-",
      "TargetUserSid": "S-1-0-0",
      "TokenElevationType": "%%1936"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 4688,
      "EventRecordID": 19237,
      "Execution": {
        "#attributes": {
          "ProcessID": 4,
          "ThreadID": 172
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13312,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:34.314920Z"
        }
      },
      "Version": 2
    }
  }
}

```

Right after the execution of `PSEXESVC.EXE`, the creation of the binary `C:\\Windows\\earnmoney.exe` is created.

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CreationUtcTime": "2021-10-17 19:08:37.742",
      "Image": "System",
      "ProcessGuid": "D5D3D798-2E2F-616C-EB03-000000000000",
      "ProcessId": 4,
      "RuleName": "EXE",
      "TargetFilename": "C:\\Windows\\earnmoney.exe",
      "UtcTime": "2021-10-17 19:08:37.742"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 11,
      "EventRecordID": 23136,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 11,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:37.752748Z"
        }
      },
      "Version": 2
    }
  }
}

```


Then, the process of `C:\\Windows\\earnmoney.exe` is created.

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"earnmoney.exe\" ",
      "MandatoryLabel": "S-1-16-16384",
      "NewProcessId": "0x17d8",
      "NewProcessName": "C:\\Windows\\earnmoney.exe",
      "ParentProcessName": "C:\\Windows\\PSEXESVC.exe",
      "ProcessId": "0x82c",
      "SubjectDomainName": "BITUP21",
      "SubjectLogonId": "0x3e7",
      "SubjectUserName": "SRV01$",
      "SubjectUserSid": "S-1-5-18",
      "TargetDomainName": "-",
      "TargetLogonId": "0x0",
      "TargetUserName": "-",
      "TargetUserSid": "S-1-0-0",
      "TokenElevationType": "%%1936"
    },
    "System": {
      "Channel": "Security",
      "Computer": "SRV01.bitup21.internal"
      "Correlation": null,
      "EventID": 4688,
      "EventRecordID": 19252,
      "Execution": {
        "#attributes": {
          "ProcessID": 4,
          "ThreadID": 172
        }
      },
      "Keywords": "0x8020000000000000",
      "Level": 0,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "54849625-5478-4994-A5BA-3E3B0328C30D",
          "Name": "Microsoft-Windows-Security-Auditing"
        }
      },
      "Security": null,
      "Task": 13312,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:37.801181Z"
        }
      },
      "Version": 2
    }
  }
}
```

```json
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "CommandLine": "\"earnmoney.exe\" ",
      "Company": "-",
      "CurrentDirectory": "C:\\Windows\\system32\\",
      "Description": "RevilLoader",
      "FileVersion": "1.0.0.0",
      "Hashes": "MD5=05F7D72C6548A3C9E521E0A001389705,SHA256=A42A8486FB334349B150D25021FE37347024233EB3B2129B8E42D1CB381385CE,IMPHASH=F34D5F2D4577ED6D9CEEC516C1F5A744",
      "Image": "C:\\Windows\\earnmoney.exe",
      "IntegrityLevel": "System",
      "LogonGuid": "D5D3D798-2E32-616C-E703-000000000000",
      "LogonId": "0x3e7",
      "OriginalFileName": "RevilLoader.exe",
      "ParentCommandLine": "C:\\Windows\\PSEXESVC.exe",
      "ParentImage": "C:\\Windows\\PSEXESVC.exe",
      "ParentProcessGuid": "D5D3D798-74B2-616C-7C01-000000001200",
      "ParentProcessId": 2092,
      "ProcessGuid": "D5D3D798-74B5-616C-7D01-000000001200",
      "ProcessId": 6104,
      "Product": "RevilLoader",
      "RuleName": "-",
      "TerminalSessionId": 0,
      "User": "NT AUTHORITY\\SYSTEM",
      "UtcTime": "2021-10-17 19:08:37.801"
    },
    "System": {
      "Channel": "Microsoft-Windows-Sysmon/Operational",
      "Computer": "SRV01.bitup21.internal",
      "Correlation": null,
      "EventID": 1,
      "EventRecordID": 23137,
      "Execution": {
        "#attributes": {
          "ProcessID": 1760,
          "ThreadID": 2360
        }
      },
      "Keywords": "0x8000000000000000",
      "Level": 4,
      "Opcode": 0,
      "Provider": {
        "#attributes": {
          "Guid": "5770385F-C22A-43E0-BF4C-06F5698FFBD9",
          "Name": "Microsoft-Windows-Sysmon"
        }
      },
      "Security": {
        "#attributes": {
          "UserID": "S-1-5-18"
        }
      },
      "Task": 1,
      "TimeCreated": {
        "#attributes": {
          "SystemTime": "2021-10-17T19:08:37.824199Z"
        }
      },
      "Version": 5
    }
  }
}
```

The execution of the binary dropped by the attacker named **`earnmoney.exe`** with **`PsExec`** took place at **`2021-10-17T19:08:37`**

And, last but not least, this entails the sixth phase of the attack which is the **DATA ENCRYPTION**:

![](images/data_encryption.png)

Then, we could see in more detail the activity on `SRV01` from the PsExec execution on the domain controller until the execution of `earnmoney.exe`.

![](images/deploy_install.png)
`

To sum up, the following picture depicts the attack as a whole:

![](images/CTF_complete.png)



### How many files were encrypted by the ransomware?

To determine the number of encrypted files on the system, we'll need to take into account the following aspects:

- **MACB timestamps:** MACB stands for modification, access, change in the MFT record, and birth (creation). When file encryption takes place, we'll need to pay attention to both the modification and change timestamps.
- **$FILENAME attribute:** The $File_Name attribute contains forensically interesting bits, such as MACB times, file name, file length, and more. This attribute can only change when we move a file location (local or volume), when we copy a file and when we create a new file.

The following poster can help us determine what's necessary to filter the MFT entries and obtain the number of encrypted files:

![](images/PosterSANS.png)

- **Encryption extension:** we'll need to filter out all the files whose extension is `ntr42`

So taking into account the aspects described above, the following command line will help us determine the number of encrypted files in the system.


 `grep '\.ntr42"$' TL.csv | grep ',m\.c\.,' | grep -v 'FILE_NAME' | wc -l`

Then, we can state that the number of encrypted files is **`216`**.

## Notes

Tools used:
- **`evtx_dump`**: A cross-platform parser for the Windows XML EventLog format - https://github.com/omerbenamram/evtx - Omer BenAmram 
- **`MFTECmd.exe`**: $MFT, $Boot, $J and $SDS parser - https://ericzimmerman.github.io/#!index.md - Eric Zimmerman's Tools 
- **`The Sleuth Kit - mactime`**: mactime creates an ASCII timeline of file activity based on a body file. - https://wiki.sleuthkit.org/index.php?title=Mactime - Brian Carrier
- **`Registry Explorer`**: Registry viewer with searching, multi-hive support, plugins, and more - https://ericzimmerman.github.io/#!index.md - Eric Zimmerman's Tools
- **`$I Parse`**: $I Parse is a tool for parsing $I (index) files from the Recycle Bin of Windows Vista and later - https://df-stream.com/recycle-bin-i-parser/ - Digital Forensics Stream.
- **`regripper`**: RegRipper is an open source tool, written in Perl, for extracting/parsing information (keys, values, data) from the Registry and presenting it for analysis. - https://github.com/keydet89/RegRipper3.0 - Harlan Carvey.

## References

- https://github.com/omerbenamram/evtx
- https://ericzimmerman.github.io/#!index.md
- https://wiki.sleuthkit.org/index.php?title=Mactime
- https://df-stream.com/recycle-bin-i-parser/
- https://github.com/keydet89/RegRipper3.0

## Author

@rociotanis
