# CTF 21

Bitup 2021 Event CTF Documentation Project.

## Index

### Web

* [**insecure2**](challenges/insecure2/)
* [**friendly-greeting**](challenges/friendly-greeting)
* [**weird-greeting**](challenges/weird-greeting)
* [**fancydevs**](challenges/fancydevs/)

## Mobile

* [**native**](challenges/native/)
* [**protected**](challenges/protected/)

## Forensic

* [**regice**](challenges/regice/)
* [**fanta**](challenges/fanta/)

## Reversing

* [**sticks-and-stones**](challenges/sticks-and-stones/)
